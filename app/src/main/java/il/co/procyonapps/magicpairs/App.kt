package il.co.procyonapps.magicpairs

import android.app.Application
import il.co.procyonapps.magicpairs.network.NumberProvider
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class App: Application() {

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("https://pastebin.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()}

    val api by lazy { retrofit.create(NumberProvider::class.java)}
}
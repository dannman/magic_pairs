package il.co.procyonapps.magicpairs.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import il.co.procyonapps.magicpairs.App
import il.co.procyonapps.magicpairs.R
import il.co.procyonapps.magicpairs.data.MatchableInt
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.absoluteValue

class MainActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProvider
            .AndroidViewModelFactory
            .getInstance(application)
            .create(MainViewModel::class.java)
    }

    private val rvAdapter = NumbersAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_main)

        rvNumbers.apply {
            adapter = rvAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
        viewModel.numArray.observe(this, numObserver)
    }

    private val numObserver = Observer<List<MatchableInt>> {
            rvAdapter.numList = it
    }

}

package il.co.procyonapps.magicpairs.data


import com.google.gson.annotations.SerializedName

data class RandomArrayResponse(
    @SerializedName("numbers") val numbers: List<Number> = listOf()
) {
    data class Number(
        @SerializedName("number") val number: Int = 0
    )
}
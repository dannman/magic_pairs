package il.co.procyonapps.magicpairs.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import il.co.procyonapps.magicpairs.R
import il.co.procyonapps.magicpairs.data.MatchableInt
import il.co.procyonapps.magicpairs.toPx
import kotlinx.android.synthetic.main.number_item.view.*

class NumbersAdapter : RecyclerView.Adapter<NumbersAdapter.NumHolder>() {


    var numList: List<MatchableInt> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NumHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.number_item, parent, false)
        return NumHolder(view)
    }

    override fun getItemCount(): Int {
        return numList.size
    }

    override fun onBindViewHolder(holder: NumHolder, position: Int) {
        holder.bind(numList[position])
    }

    inner class NumHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(num: MatchableInt) {
            view.tvNumber.text = "${num.value}"

             view.layoutParams.apply {
                 height = if(num.hasMatch) 100.toPx else 50.toPx
             }
        }
    }

}
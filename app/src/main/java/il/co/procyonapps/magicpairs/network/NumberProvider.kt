package il.co.procyonapps.magicpairs.network

import il.co.procyonapps.magicpairs.data.RandomArrayResponse

import retrofit2.http.GET

interface NumberProvider {

    @GET("/raw/8wJzytQX")
    suspend fun getRandomNumArray(): RandomArrayResponse
}
package il.co.procyonapps.magicpairs.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import il.co.procyonapps.magicpairs.App
import il.co.procyonapps.magicpairs.data.MatchableInt
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.math.absoluteValue

class MainViewModel (app: Application) : AndroidViewModel(app) {



    val numArray: LiveData<List<MatchableInt>> = liveData (Dispatchers.IO){

        val response = (getApplication() as App).api.getRandomNumArray()

        val matched = sortAndMatch(response.numbers.map { it.number })

        emit(matched)
    }

    private suspend fun sortAndMatch(source: List<Int>): List<MatchableInt> =
        withContext(Dispatchers.Default) {
            val sorted = source
                .toSortedSet()
                .map { MatchableInt(it) }

            var smallest = 0
            var largest = sorted.size - 1

            while (smallest < largest) {
                when {
                    sorted[smallest].value.absoluteValue == sorted[largest].value.absoluteValue -> {
                        sorted[smallest].hasMatch = true
                        sorted[largest].hasMatch = true
                        smallest++
                        largest--
                    }
                    sorted[smallest].value.absoluteValue > sorted[largest].value.absoluteValue -> {
                        smallest++
                    }
                    sorted[smallest].value.absoluteValue < sorted[largest].value.absoluteValue -> {
                        largest--
                    }
                }
            }
            sorted
        }
}
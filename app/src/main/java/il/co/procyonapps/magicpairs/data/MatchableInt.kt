package il.co.procyonapps.magicpairs.data

data class MatchableInt(val value : Int, var hasMatch: Boolean = false)